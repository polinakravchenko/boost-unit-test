#include "tabulate_func_test.h"
#include <numeric>

class MockRealCalculator : public Calculator<double, double> {
public:
    MOCK_CONST_METHOD1_T(calculate, double(const double& x));
};

TEST(GTabulatorTestSuite, ShouldSuccessTabulate)
{
    using ::testing::_;
    using ::testing::Return;
    auto a = 1.0, b = 2.0;
    auto count = 5;
    auto stubResult = 1.0;
    MockRealCalculator calculator;
    EXPECT_CALL(calculator, calculate(_))
        .Times(count)
        .WillRepeatedly(Return(stubResult));

    auto tabulator = Tabulator(calculator);
    auto result = tabulator.tabulate(a, b, count);
    ASSERT_EQ(count, result.size());
    ASSERT_DOUBLE_EQ(stubResult * count, std::accumulate(result.begin(), result.end(), 0.0));
}

MockRealCalculator::MOCK_CONST_METHOD1_T(calculate, double(const double& x))
{
}
