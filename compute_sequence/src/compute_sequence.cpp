#include "compute_sequence.h"

double sequence_multiply(double x, int n) {
    return -SQR(x) * (2 * n - 1) / (2 * n + 1);
}

double etalon_func(double x)
{
    return atan(x) / SQR(x);
}
