#include <iostream>
#include "../../common_math/include/common_math.h"

#define _USE_MATH_HELPERS_


double func(double, double);

void tabulate_func (common::math::func2_t, double, double, double, double, int nx = 10, int ny = 10);